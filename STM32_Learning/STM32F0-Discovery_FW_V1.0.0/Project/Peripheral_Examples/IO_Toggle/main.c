/**
  ******************************************************************************
  * @file    IO_Toggle/main.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    23-March-2012
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"

/** @addtogroup STM32F0_Discovery_Peripheral_Examples
  * @{
  */

/** @addtogroup IO_Toggle
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define BSRR_VAL        0x0300

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef        GPIO_InitStructure;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f0xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f0xx.c file
     */
	int i, j;
  /* GPIOC Periph clock enable */
  //RCC: Reset clock control 
  //GPIOC: 因為這裡要點燈, 這塊板子的燈是PC8和PC9, 意思是把IO的Port_C做Enable
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE); 

  /* Configure PC8 and PC9 in output pushpull mode */
  //設定Pin8和Pin9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
  //設定MODE~ IN:輸入, OUT:輸出, AF:輸入輸出以外的功能(通訊接口), AN:做ADC/DAC的時候會用
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  //PP(Push-Pull Circuit):總是會有一個狀態
  //OD(Open Collector Circuit): 0:可能會是一個浮動的電壓, 1:GND
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  //設定速度 通常選最快的 越慢只是越省電
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //設定是否上下拉 UP:上拉, DOWN:下拉, NOPULL:沒有上下拉
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /* To achieve GPIO toggling maximum frequency, the following  sequence is mandatory. 
     You can monitor PC8 and PC9 on the scope to measure the output signal. 
     If you need to fine tune this frequency, you can add more GPIO set/reset 
     cycles to minimize more the infinite loop timing.
     This code needs to be compiled with high speed optimization option.  */
	while (1)
  {
    GPIO_SetBits(GPIOC, GPIO_Pin_8);
		for(j=0; j<20; j++)
		{
			for(i=0; i<0xFFFF; i++);
		}
		GPIO_ResetBits(GPIOC, GPIO_Pin_8);
		for(j=0; j<20; j++)
		{
			for(i=0; i<0xFFFF; i++);
		}
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
